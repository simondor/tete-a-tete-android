package com.fineice.android.app.utility;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

import com.fineice.android.app.common.Fonts;

/**
 * Utility class that matches no category.
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public final class Utility {

    /**
     * Disable class instantiation.
     */
    private Utility() {
        throw new AssertionError("No instances");
    }

    /**
     * Applies custom {@link android.graphics.Typeface} into supplied {@link EditText} instance.
     * @param target supplied target
     * @param attrs supplied attributes
     * @param styleableResId supplied custom defined attribute styles
     * @param index supplied custom style name
     */
    public static void applyFont(final TextView target,
                                 final AttributeSet attrs,
                                 final int[] styleableResId,
                                 final int index) {
        final Context context = target.getContext();
        TypedArray array = context.obtainStyledAttributes(attrs, styleableResId);
        if (array != null) {
            String font = array.getString(index);
            if (!TextUtils.isEmpty(font) && !target.isInEditMode()) {
                target.setTypeface(Fonts.getFont(context, font));
            }
            array.recycle();
        }
    }
}
