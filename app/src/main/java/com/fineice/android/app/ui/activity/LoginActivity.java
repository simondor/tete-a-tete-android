package com.fineice.android.app.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fineice.android.app.R;

/**
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public class LoginActivity extends NetworkActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }
}
