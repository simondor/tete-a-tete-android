package com.fineice.android.app.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public class CreateAuctionActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayout() {
        return 0;
    }
}
