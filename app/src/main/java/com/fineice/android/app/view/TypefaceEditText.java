package com.fineice.android.app.view;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.fineice.android.app.R;
import com.fineice.android.app.utility.Utility;

/**
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public final class TypefaceEditText extends AppCompatEditText {

    public TypefaceEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Utility.applyFont(
                this,
                attrs,
                R.styleable.TypefaceEditText,
                R.styleable.TypefaceEditText_font);
    }
}
