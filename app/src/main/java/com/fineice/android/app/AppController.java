package com.fineice.android.app;

import android.app.Application;

import com.fineice.android.app.common.Endpoints;
import com.fineice.android.app.di.AppComponent;
import com.fineice.android.app.di.AppModule;
import com.fineice.android.app.di.DaggerAppComponent;
import com.fineice.android.app.di.NetModule;

import timber.log.Timber;

/**
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public class AppController extends Application {

    // properties
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(Endpoints.URL))
                .build();
        // enable logging for debug builds only
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public final AppComponent getAppComponent() {
        return mAppComponent;
    }
}
