package com.fineice.android.app.helper;

import android.content.SharedPreferences;

import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Helper class used to simplify and manage work with {@link SharedPreferences} across application.
 * That class is injected by Dagger so should be injected in every used place.
 * Class is being kept as singleton.
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
@Singleton
public final class SharedPrefsHelper {

    // properties
    private SharedPreferences mPrefs;

    @Inject
    public SharedPrefsHelper(final SharedPreferences prefs) {
        mPrefs = prefs;
    }

    /**
     * @param key key of preference to check
     * @return true if preference is stored, false otherwise
     */
    public final boolean contains(final String key) {
        return mPrefs.contains(key);
    }

    /**
     * Removes specific preference.
     * @param key supplied key of preference to remove
     */
    public final void remove(final String key) {
        mPrefs.edit().remove(key).apply();
    }

    public final void putInt(final String key, final int value) {
        mPrefs.edit().putInt(key, value).apply();
    }

    public final void putFloat(final String key, final float value) {
        mPrefs.edit().putFloat(key, value).apply();
    }

    public final void putLong(final String key, final long value) {
        mPrefs.edit().putLong(key, value).apply();
    }

    public final void putBoolean(final String key, final boolean value) {
        mPrefs.edit().putBoolean(key, value).apply();
    }

    public final void putString(final String key, final String value) {
        mPrefs.edit().putString(key, value).apply();
    }

    public final void putStringSet(final String key, final Set<String> value) {
        mPrefs.edit().putStringSet(key, value).apply();
    }

    public final int getInt(final String key) {
        return mPrefs.getInt(key, -1);
    }

    public final int getInt(final String key, final int defValue) {
        return mPrefs.getInt(key, defValue);
    }

    public final float getFloat(final String key) {
        return mPrefs.getFloat(key, -1f);
    }

    public final float getFloat(final String key, final float defValue) {
        return mPrefs.getFloat(key, defValue);
    }

    public final long getLong(final String key) {
        return mPrefs.getLong(key, -1);
    }

    public final long getLong(final String key, final long defValue) {
        return mPrefs.getLong(key, defValue);
    }

    public final boolean getBoolean(final String key) {
        return mPrefs.getBoolean(key, false);
    }

    public final boolean getBoolean(final String key, final boolean defValue) {
        return mPrefs.getBoolean(key, defValue);
    }

    public final String getString(final String key) {
        return mPrefs.getString(key, null);
    }

    public final String getString(final String key, final String defValue) {
        return mPrefs.getString(key, defValue);
    }

    public final Set<String> getStringSet(final String key) {
        return mPrefs.getStringSet(key, null);
    }

    public final Set<String> getStringSet(final String key, final Set<String> defValue) {
        return mPrefs.getStringSet(key, defValue);
    }
}
