package com.fineice.android.app.di;

import android.content.Context;

import com.fineice.android.app.client.ApiService;
import com.fineice.android.app.helper.SharedPrefsHelper;
import com.fineice.android.app.ui.activity.BaseActivity;
import com.fineice.android.app.ui.activity.NetworkActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Main component used to provide injected dependencies.
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {

    ApiService getApiService();
    SharedPrefsHelper getSharedPrefsHelper();
    @ApplicationContext
    Context getAppContext();

    /**
     * Injects specified dependencies.
     * @param target target activity where dependencies are being injected.
     */
    void inject(BaseActivity target);

    /**
     * Injects specified dependencies.
     * @param target target activity where dependencies are being injected.
     */
    void inject(NetworkActivity target);
}
