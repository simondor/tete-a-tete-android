package com.fineice.android.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.fineice.android.app.model.Notification;

import butterknife.ButterKnife;

/**
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public class NotificationsAdapter extends Adapter<Notification> {

    NotificationsAdapter(Context context) {
        super(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder source, int position) {
        ViewHolder holder = (ViewHolder) source;
        final Notification item = mItems.get(position);
        // TODO implement
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(final View root) {
            super(root);
            ButterKnife.bind(this, root);
        }
    }
}
