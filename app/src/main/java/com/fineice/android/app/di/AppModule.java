package com.fineice.android.app.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Module used to provide application related objects which will be injected by dagger.
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
@Module
public class AppModule {

    private final Application mApplication;

    public AppModule(final Application application) {
        mApplication = application;
    }

    @Singleton
    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @ApplicationContext
    @Singleton
    @Provides
    Context provideContext() {
        return mApplication;
    }

    @Singleton
    @Provides
    SharedPreferences getSharedPreferences() {
        return mApplication.getSharedPreferences("app-prefs", Context.MODE_PRIVATE);
    }
}
