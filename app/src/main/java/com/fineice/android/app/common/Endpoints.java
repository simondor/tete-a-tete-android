package com.fineice.android.app.common;

/**
 * Common class where are stored server related endpoints.
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public final class Endpoints {

    public static final String URL = "";

    /**
     * Disable class instantiation.
     */
    private Endpoints() {
        throw new AssertionError("No instances");
    }
}
