package com.fineice.android.app.view;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.fineice.android.app.R;
import com.fineice.android.app.utility.Utility;

/**
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public final class TypefaceTextView extends AppCompatTextView {

    public TypefaceTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Utility.applyFont(
                this,
                attrs,
                R.styleable.TypefaceTextView,
                R.styleable.TypefaceTextView_font);
    }
}
