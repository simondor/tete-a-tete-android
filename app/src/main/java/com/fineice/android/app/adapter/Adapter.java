package com.fineice.android.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public abstract class Adapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // properties
    protected WeakReference<Context> mContextRef;
    protected List<T> mItems;

    Adapter(final Context context) {
        mContextRef = new WeakReference<>(context);
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    /**
     * Updates current adapter instance with supplied data collection.
     * @param data supplied data collection to update
     */
    protected final void update(final List<T> data) {
        if (data == null || data.size() == 0) {
            return;
        }
        if (mItems.size() > 0) {
            int count = getItemCount();
            mItems.clear();
            notifyItemRangeRemoved(0, count);
        }
        mItems.addAll(data);
        notifyItemRangeInserted(0, data.size());
    }

    /**
     * Appends supplied data collection into current adapter.
     * @param data supplied data collection to append
     */
    protected final void append(final List<T> data) {
        if (data == null || data.size() == 0) {
            return;
        }
        int startPosition = getItemCount();
        mItems.addAll(data);
        notifyItemRangeInserted(startPosition, data.size());
    }

    /**
     * Updates current adapter instance with supplied data object.
     * @param data supplied data object to update
     */
    protected final void update(final T data) {
        if (data == null) {
            return;
        }
        if (mItems.size() > 0) {
            int count = getItemCount();
            mItems.clear();
            notifyItemRangeRemoved(0, count);
        }
        mItems.add(data);
        notifyItemInserted(0);
    }
}
