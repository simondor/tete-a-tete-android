package com.fineice.android.app.common;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;

/**
 * Common class used to simplify and provide custom fonts used across application.
 * It uses {@link android.support.v4.util.LruCache} as cache.
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public final class Fonts {

    // properties
    private static final LruCache<String, Typeface> FONTS = new LruCache<>(5);

    /**
     * Disable class instantiation.
     */
    private Fonts() {
        throw new AssertionError("No instances");
    }

    /**
     * @param context caller context
     * @param fontName name of font to get
     * @return {@link Typeface} instance created from assets
     */
    public static Typeface getFont(@NonNull final Context context,
                                   @NonNull final String fontName) {
        Typeface font = FONTS.get(fontName);
        if (font == null) {
            font = Typeface.createFromAsset(context.getAssets(), fontName);
            FONTS.put(fontName, font);
        }
        return font;
    }
}
