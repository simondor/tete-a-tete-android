package com.fineice.android.app.di;

import android.app.Application;
import android.support.annotation.NonNull;

import com.fineice.android.app.client.ApiService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Module used to provide network related objects which will be injected by dagger.
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
@Module
public class NetModule {

    private final String mUrl;

    public NetModule(final String url) {
        mUrl = url;
    }

    @Singleton
    @Provides
    Cache provideOkHttpCache(@NonNull Application application) {
        return new Cache(application.getCacheDir(), 10 * 1024 * 1024);
    }

    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient(@NonNull Cache cache) {
        // there is place for putting auth interceptor when needed
        return new OkHttpClient.Builder()
                .cache(cache)
                .build();
    }

    @Singleton
    @Provides
    Gson provideGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(@NonNull OkHttpClient client, @NonNull Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(mUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Singleton
    @Provides
    ApiService provideApiService(@NonNull Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }
}
