package com.fineice.android.app.common;

/**
 * Common class used to store all used constants across application.
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public final class Constants {

    /**
     * Disable class instantiation
     */
    private Constants() {
        throw new AssertionError("No instances");
    }
}
