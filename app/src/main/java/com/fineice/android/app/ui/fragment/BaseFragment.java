package com.fineice.android.app.ui.fragment;

/**
 * A base class from which should extend all other fragments.
 * @author Simon Dorociak <S.Dorociak@gmail.com>
 */
public abstract class BaseFragment {
}
